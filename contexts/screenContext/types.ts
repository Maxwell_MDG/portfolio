export enum SCREEN_TYPE {
    SKILLS = 'Skills',
    HOBBY = 'Hobby Projects',
    ENTERPRISE = 'Enterprise Projects',
}