export const COLORS = {
    Autumn: {
        primary: "#9C6A49",
        secondary: "#8C8681",
        tertiary: "#AEAAAC"
    },
    Jungle: {
        primary: "#83A02A",
        secondary: "#555B2A",
        tertiary: "#304516"
    },
    Mountains: {
        primary: "#AEB2CA",
        secondary: "#6F7EA6",
        tertiary: "#404967"
    }
}